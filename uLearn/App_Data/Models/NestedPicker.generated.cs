//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	// Mixin content Type 1158 with alias "nestedPicker"
	/// <summary>Nested Picker</summary>
	public partial interface INestedPicker : IPublishedContent
	{
		/// <summary>Nested</summary>
		IEnumerable<IPublishedContent> Nested { get; }
	}

	/// <summary>Nested Picker</summary>
	[PublishedContentModel("nestedPicker")]
	public partial class NestedPicker : PublishedContentModel, INestedPicker
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "nestedPicker";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public NestedPicker(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<NestedPicker, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Nested
		///</summary>
		[ImplementPropertyType("nested")]
		public IEnumerable<IPublishedContent> Nested
		{
			get { return GetNested(this); }
		}

		/// <summary>Static getter for Nested</summary>
		public static IEnumerable<IPublishedContent> GetNested(INestedPicker that) { return that.GetPropertyValue<IEnumerable<IPublishedContent>>("nested"); }
	}
}
